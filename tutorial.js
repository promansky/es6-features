let birds = [
    { name: 'Roko', type: 'cockatiel', age: 1 },
    { name: 'Bibi', type: 'canary', age: 2 },
    { name: 'Pumpkin', type: 'budgie', age: 2 },
    { name: 'Gizmo', type: 'cockatiel', age: 3 }
]

let cockatielsArray = [];
for(let i=0; i<birds.length; i++){
    if (birds[i].type =='cockatiel'){
        cockatielsArray.push(birds[i]);
    }
}

let cockatiels = birds.filter(animal => animal.type === 'cockatiel');

let namesArray = [];
for(let i=0; i<birds.length; i++){
        namesArray.push(birds[i].name);
}

let names = birds.map(bird => bird.name)

let birdsTotalAge = 0;
for(let i=0; i<birds.length; i++){
    birdsTotalAge+=birds[i].age; 
}

let ageReduce = birds.reduce(function(start,bird){
    return start + bird.age;
},0);

console.log('\nBirds that are cockatiels: ',JSON.stringify(cockatiels,null,2));
console.log('\nList of bird names: ', names);
console.log('\nTotal age of birds: ', ageReduce);